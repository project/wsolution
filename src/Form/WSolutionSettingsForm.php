<?php

namespace Drupal\wsolution\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductPageForm.
 */
abstract class WSolutionSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config_name = $this->getEditableConfigNames();
    if (!empty($config_name)) {
      $conf = $this->config(reset($config_name));
      foreach ($form_state->getValues() as $key => $value) {
        $conf->set($key, $value);
      }
      $conf->save();
    }
  }

}

<?php

namespace Drupal\wsolution\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductPageForm.
 */
class OutgoingSettingsForm extends WSolutionSettingsForm {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wsolution.outgoing.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wsolution_outgoing_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wsolution.outgoing.settings');
    $form['library'] = [
      '#access' => FALSE,
      '#title' => t('Library Hooks'),
      '#description' => t('Library hooks allow PHP-VCR to intercept HTTP requests by overwriting the libraries method to issue requests0'),
      '#default_value' => $config->get('library') ?? [
          'curl',
          'soap',
          'stream_wrapper',
        ],
      '#options' => [
        'curl' => t('Curl'),
        'soap' => t('Soap'),
        'stream_wrapper' => t('Stream wrapper'),
      ],
    ];

    $form['explanation'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Please fill in the information below to configure the module.'),
    ];

    // Onglet 'WSID WS'
    $form['wsidws']['explanation'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Go to wsolution.io create your account then copy your uuid on the account edit page.'),
    ];
    $form['wsidws']['wsid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('UUID'),
      '#default_value' => $config->get('wsid'),
    ];

    // Onglet 'Pattern URL'
    $form['rest_url']['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => ['include' => $this->t('Include'), 'exclude' => $this->t('Exclude')],
      '#default_value' => $config->get('mode'),
    ];
    $form['rest_url']['rest_url'] = [
      '#type' => 'textarea',
      '#title' => t('Rest URLs'),
      '#description' => t('Handle all rest request by matching url. One url per line.'),
      '#default_value' => $config->get('rest_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

}

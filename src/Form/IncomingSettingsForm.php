<?php

namespace Drupal\wsolution\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProductPageForm.
 */
class IncomingSettingsForm extends WSolutionSettingsForm {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wsolution.incoming.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wsolution_incoming_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wsolution.incoming.settings');
    $form['rest_plugin'] = [
      '#type' => 'checkbox',
      '#title' => t('Handle rest plugin'),
      '#description' => t('Handle all rest plugin implements in Drupal.'),
      '#default_value' => $config->get('rest_plugin') ?? TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

}

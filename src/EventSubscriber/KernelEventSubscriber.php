<?php

namespace Drupal\wsolution\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\wsolution\Services\WSolutionInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * The Drupal configuration service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * @var \Drupal\wsolution\Services\WSolution
   */
  protected $wsolutionManager;

  /**
   * Constructs a new KernelEventSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal configuration service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Drupal logger service.
   * @param \Drupal\wsolution\Services\WSolutionInterface $wsolution_manager
   *   The Drupal configuration service.
   */
  public function __construct(WSolutionInterface $wsolution_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    // Setup object members.
    $this->wsolutionManager = $wsolution_manager;
  }

  /**
   * Gets the 'wsid' from the 'wsolution.outgoing.settings' configuration.
   *
   * @return mixed
   *   The 'wsid' configuration value.
   */
  protected function getWsid() {
    try {
      $config = $this->configFactory->get('wsolution.outgoing.settings');
      return $config->get('wsid');
    }
    catch (\Exception $e) {
      // Log the error.
      $this->loggerFactory->get('wsolution')->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['kernelRequest', 100];
    $events[KernelEvents::TERMINATE][] = ['kernelTerminate'];

    return $events;
  }

  /**
   * React when kernel recieved request.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   */
  public function kernelRequest(RequestEvent $event) {
    // Check wether WSolution ID exists.
    $wsid = $this->getWsid();
    if (empty($wsid)) {
      return;
    }

    $request = $event->getRequest();
    // For the moment desactivate incoming on REST.
    // @todo : nettoyer la partie incoming.
    //$this->wsolutionManager->startListeningIncoming($request);
    $this->wsolutionManager->startListeningOutgoing();
  }

  /**
   * React when kernel terminate request.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   */
  public function kernelTerminate(TerminateEvent $event) {
    // Check wether WSolution ID exists.
    $wsid = $this->getWsid();
    if (empty($wsid)) {
      return;
    }

    $this->wsolutionManager->stopListeningOutgoing();
    // For the moment desactivate incoming on REST.
    // @todo : nettoyer la partie incoming.
    //$this->wsolutionManager->stopListeningIncoming();
  }

}

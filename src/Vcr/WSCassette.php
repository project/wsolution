<?php

namespace Drupal\wsolution\Vcr;

use VCR\Cassette;
use VCR\Request;
use VCR\Response;

/**
 * A Cassette records and plays back pairs of Requests and Responses in a
 * Storage.
 */
class WSCassette extends Cassette {

  protected $mode;

  public function setMode($mode) {
    $this->mode = $mode;
  }
  /**
   * We do not record anything, just modifying on-the-fly.
   *
   * @param Request $request request to record
   * @param Response $response response to record
   */
  public function record(Request $request, Response $response, int $index = 0): void {

  }

}

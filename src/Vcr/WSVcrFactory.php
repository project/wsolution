<?php

namespace Drupal\wsolution\Vcr;

use VCR\Configuration;
use VCR\VCRFactory;

class WSVcrFactory extends VCRFactory {

  /**
   * @var Configuration
   **/
  protected Configuration $config;

  /**
   * @var array<string, object>
   */
  protected array $mapping = [];

  /**
   * @var self|null
   */
  protected static ?VCRFactory $instance = null;

  /**
   * Creates a new VCRFactory instance.
   *
   * @param Configuration $config
   */
  protected function __construct(Configuration $config = NULL) {
    $this->config = $config ?? new Configuration(); // Consider providing a default Configuration object if null is passed
    parent::__construct($this->config);
  }

  /**
   * Returns the same VCRFactory instance on ever call (singleton).
   *
   * @param Configuration $config (Optional) configuration
   *
   * @return VCRFactory
   */
  public static function getInstance(Configuration $config = NULL): self {
    if (!self::$instance) {
      self::$instance = new self($config);
    }

    return self::$instance;
  }

  /**
   * Returns an instance for specified class name and parameters.
   *
   * @param string $className class name to get a instance for
   * @param mixed[] $params constructor arguments for this class
   *
   * @return mixed an instance for specified class name and parameters
   */
  public static function get(string $className, array $params = []): mixed {
    return self::getInstance()->getOrCreate($className, $params);
  }

  /**
   * Returns an instance for specified classname and parameters.
   *
   * @param string $className class name to get a instance for
   * @param mixed[] $params constructor arguments for this class
   *
   * @return mixed
   */
  public function getOrCreate(string $className, array $params = []): mixed {
    $key = $className . implode('-', $params);

    if (isset($this->mapping[$key])) {
      return $this->mapping[$key];
    }

    $callable = [$this, $this->getMethodName($className)];

    if (\is_callable($callable)) {
      $instance = \call_user_func_array($callable, $params);
    }
    else {
      $instance = new $className();
    }

    return $this->mapping[$key] = $instance;
  }

  /**
   * @return \Drupal\wsolution\Vcr\WSVideorecorder
   */
  protected function createDrupalwsolutionVcrWSVideorecorder(): WSVideorecorder {
    return new WSVideorecorder(
      $this->config,
      $this->getOrCreate('VCR\Util\HttpClient'),
      $this
    );
  }

}
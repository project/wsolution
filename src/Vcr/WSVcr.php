<?php

namespace Drupal\wsolution\Vcr;

use Assert\Assertion;
use VCR\VCR;

/**
 * Singleton interface to a Videorecorder.
 *
 * @method static Configuration configure()
 * @method static void insertCassette(string $cassetteName)
 * @method static void setBlacklist( $cassetteName)
 * @method static void setMode( $cassetteName)
 * @method static void turnOn()
 * @method static void turnOff()
 * @method static void eject()
 * @mixin WSVideorecorder
 */
class WSVcr extends VCR {


  /**
   * @param mixed[] $parameters
   *
   * @return mixed
   * @dev need to be override to insert ou factory and videorecorder.
   */
  public static function __callStatic(string $method, array $parameters): mixed {
    $callable = [WSVcrFactory::get(WSVideorecorder::class), $method];

    Assertion::isCallable($callable);

    return \call_user_func_array($callable, $parameters);
  }

}
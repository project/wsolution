<?php

namespace Drupal\wsolution\Vcr;

use VCR\Event\AfterHttpRequestEvent;
use VCR\Event\AfterPlaybackEvent;
use VCR\Event\BeforeHttpRequestEvent;
use VCR\Event\BeforePlaybackEvent;
use VCR\Event\BeforeRecordEvent;
use VCR\Event\Event;
use VCR\Request;
use VCR\Response;
use VCR\Util\Assertion;
use VCR\Util\CurlException;
use VCR\VCR;
use VCR\VCREvents;
use VCR\Videorecorder;
use Drupal\Core\Config\ConfigFactoryInterface;

class WSVideorecorder extends Videorecorder {

  public function setMode($mode) {
    if (NULL === $this->cassette) {
      throw new \BadMethodCallException('Invalid http request. No cassette inserted. ' . 'Please make sure to insert a cassette in your unit test using ' . "VCR::insertCassette('name');");
    }
    $this->cassette->setMode($mode);
  }

  public function insertCassette(string $cassetteName): void {
    Assertion::true($this->isOn, 'Please turn on VCR before inserting a cassette, use: VCR::turnOn().');

    if (NULL !== $this->cassette) {
      $this->eject();
    }

    $storage = $this->factory->get('Storage', [$cassetteName]);

    // Construct Cassette with our class.
    $this->cassette = new WSCassette($cassetteName, $this->config, $storage);
    $this->enableLibraryHooks();
  }

  /**
   * Records, sends or plays back a intercepted request.
   *
   * If a request was already recorded on a cassette it's response is returned,
   * otherwise the request is issued and it's response recorded (and returned).
   *
   * @param Request $request intercepted request
   *
   * @return Response response for the intercepted request
   *
   * @throws \BadMethodCallException if there was no cassette inserted
   * @throws \LogicException         if the mode is set to none or once and
   *                                 the cassette did not have a matching
   *   response
   * @api
   *
   */
  public function handleRequest(Request $request): Response {

    if (NULL === $this->cassette) {
      throw new \BadMethodCallException('Invalid http request. No cassette inserted. ' . 'Please make sure to insert a cassette in your unit test using ' . "VCR::insertCassette('name');");
    }

    $urlParts = parse_url($request->getUrl());

    $configs = $this->getConfigs();

    // Test wether pattern match config. Returns TRUE or FALSE.
    $matchPattern = $this->testPattern($urlParts, $configs);

    // If the urls match, we transform the url into a webservice
    if ($matchPattern) {
      $url = $this->modifyHost($urlParts,$configs);

      // Sets the new URL.
      $request->setUrl($url);

      // Reparse the new URL.
      $urlParts = parse_url($request->getUrl());
    }

    // VCR adds host header that must be changed into the new host.
    $request->setHeader('Host', $urlParts['host']);
    $event = new BeforePlaybackEvent($request, $this->cassette);
    $this->dispatch($event, VCREvents::VCR_BEFORE_PLAYBACK);

    $response = $this->cassette->playback($request);

    // Playback succeeded and the recorded response can be returned.
    if (!empty($response)) {
      $event = new AfterPlaybackEvent($request, $response, $this->cassette);
      $this->dispatch($event, VCREvents::VCR_AFTER_PLAYBACK);

      return $response;
    }

    if (VCR::MODE_NONE === $this->config->getMode()
      || VCR::MODE_ONCE === $this->config->getMode()
      && FALSE === $this->cassette->isNew()
    ) {
      throw new \LogicException(sprintf("The request does not match a previously recorded request and the 'mode' is set to '%s'. " . "If you want to send the request anyway, make sure your 'mode' is set to 'new_episodes'. " . 'Please see http://php-vcr.github.io/documentation/configuration/#record-modes.' . "\nCassette: %s \n Request: %s", $this->config->getMode(), $this->cassette->getName(), print_r($request->toArray(), TRUE)));
    }

    $this->disableLibraryHooks();

    try {
      $this->dispatch(new BeforeHttpRequestEvent($request), VCREvents::VCR_BEFORE_HTTP_REQUEST);
      $response = $this->client->send($request);

      $this->dispatch(new AfterHttpRequestEvent($request, $response), VCREvents::VCR_AFTER_HTTP_REQUEST);

      $this->dispatch(new BeforeRecordEvent($request, $response, $this->cassette), VCREvents::VCR_BEFORE_RECORD);
    }
    catch (CurlException $e) {
      $info = $e->getInfo();
      $status = [
        'http_version' => $info['http_version'] ?? 0,
        'code' => $info['http_code'] ?? 0,
        'message' => $e->getMessage(),
      ];
      $response = new Response($status, [], NULL, $info);
    }
    finally {
      $this->cassette->record($request, $response);
      $this->enableLibraryHooks();
    }

    return $response;
  }

  /**
   * Retrieves user's saved configurations.
   *
   * @return array $configs An array containing the user's saved configurations.
   */
  public function getConfigs() {

    /**
     * The Drupal configuration service.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    // @todo Refactor this code to inject dependencies into service classes
    // or controllers rather than using \Drupal::service().
    $config_factory = \Drupal::service('config.factory');
    $config_outgoing = $config_factory->get('wsolution.outgoing.settings');

    // Retrieve config URL
    $urlsConfigsString = $config_outgoing->get('rest_url');

    // Convert string in array
    $configs['urls'] = explode("\n", $urlsConfigsString);

    // Get the wsid
    $configs['wsid'] = $config_outgoing->get('wsid');

    // Get the mode include or exclude
    $configs['mode'] = $config_outgoing->get('mode');

    // Default scheme is https
    $configs['scheme'] = $configs['scheme'] ?? \Drupal::config('wsolution.outgoing.settings')->get('scheme') ?? 'https://';

    // Default domain to call is wsolution.app
    $configs['domain_ws'] = $configs['domain_ws'] ?? \Drupal::config('wsolution.outgoing.settings')->get('domain_ws') ?? '.wsolution.app';

    return $configs;
  }

  /**
   * Modifies host according to config.
   *
   * @param $urlParts The parsed URL of the current request
   * @param $configs Array containing user's saved configurations
   *
   * @return string $url String modified for WS
   */
  public function modifyHost($urlParts,$configs) {
    // htt_build_url need PELC library so do it manually.
    //$url = http_build_url($urlParts);

    $scheme =  $configs['scheme'];

    $host = str_replace(['.'], ['--'], $urlParts['host']) . '---' .  $configs['wsid'] . $configs['domain_ws'];

    if ($urlParts['scheme'] == 'http') {
      $host = 'http---' . $host;
    }

    $port = isset($urlParts['port']) ? ':' . $urlParts['port'] : '';
    $user = isset($urlParts['user']) ? $urlParts['user'] : '';
    $pass = isset($urlParts['pass']) ? ':' . $urlParts['pass']  : '';
    $pass = ($user || $pass) ? "$pass@" : '';
    $path = isset($urlParts['path']) ? $urlParts['path'] : '';
    $query = isset($urlParts['query']) ? '?' . $urlParts['query'] : '';
    $fragment = isset($urlParts['fragment']) ? '#' . $urlParts['fragment'] : '';

    $url = $scheme . $user . $pass . $host . $port . $path . $query . $fragment;

    return $url;
  }


  /**
   * Dispatches and modifies URLs according to config.
   *
   * @param $urlParts The parsed URL of the current request
   * @param $configs Array containing user's saved configurations
   *
   * @return boolean If there's a match, return TRUE
   */
  public function testPattern($urlParts, $configs) {

    $parsedUrlsConfigs = [];
    $verifParsedUrlExist = FALSE;

    // Check if 'domain_ws' exists in the configs and if 'host' exists in urlParts.
    if (!isset($configs['domain_ws']) || !isset($urlParts['host'])) {
      return FALSE;
    }

    // Check if 'domain_ws' is in 'host'.
    if (strpos($urlParts['host'], $configs['domain_ws']) !== FALSE) {
      return FALSE;
    }

    // If every path is included.
    if ($configs['mode'] == 'include' && count($configs['urls']) == 0) {
      return TRUE;
    }

    // Loop through the pattern of URLs provided by the user and split each URL with host and path
    foreach($configs['urls'] as $urlConfigs) {
      // Parse the URL
      $partsConfigs = parse_url(trim($urlConfigs)); // Use trim() to eliminate any possible white spaces

      // Check if the "host" and the "path" exist otherwise create them
      if (isset($partsConfigs['host']) && isset($partsConfigs['path'])) {
        $parsedUrlsConfigs[] = [
          'host' => $partsConfigs['host'],
          'path' => $partsConfigs['path'],
        ];
      }
    }

    // Loop through the pattern of URLs provided by the user and then compare it with the current call
    foreach($parsedUrlsConfigs as $parsedUrlConfigs) {
      if($urlParts['host'] == $parsedUrlConfigs['host'] && $urlParts['path'] == $parsedUrlConfigs['path']) {
        if($configs['mode'] == 'exclude'){
          $verifParsedUrlExist = FALSE;
        }else{
          $verifParsedUrlExist = TRUE;
        }
      }
    }

    return (($configs['mode'] && $verifParsedUrlExist) || (!$configs['mode'] && !$verifParsedUrlExist));
  }

  /**
   * Dispatches an event to all registered listeners.
   *
   * @param Event $event the event to pass to the event handlers/listeners
   * @param string $eventName the name of the event to dispatch
   */
  private function dispatch(Event $event, $eventName = NULL): Event {
    if (class_exists(\Symfony\Component\EventDispatcher\Event::class)) {
      $res = $this->getEventDispatcher()->dispatch($eventName, $event);

      Assertion::isInstanceOf($res, Event::class);

      return $res;
    }

    $res = $this->getEventDispatcher()->dispatch($event, $eventName);

    Assertion::isInstanceOf($res, Event::class);

    return $res;
  }

}
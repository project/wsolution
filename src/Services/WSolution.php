<?php

namespace Drupal\wsolution\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\wsolution\Passeplat\PassePlatComponent;
use Drupal\wsolution\Passeplat\StreamProcessor\SchemeProcessor\VCRSchemeProcessor;
use Drupal\wsolution\Vcr\WSVcr;
use Drupal\wsolution\Passeplat\EventListener\EventListenerDefinition;
use Symfony\Component\HttpFoundation\Request;
use PassePlat\Core\StreamProcessor\SchemeProcessor\Event\ProcessSchemeEvent;
use PassePlat\Core\Event\ProcessedRequestEvent;

/**
 * Class devisService.
 */
class WSolution implements WSolutionInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempstorePrivate;

  /**
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore_private
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   */
  public function __construct(ConfigFactoryInterface $config_factory, PrivateTempStoreFactory $tempstore_private, RouteProviderInterface $route_provider, PathMatcherInterface $path_matcher) {
    // Setup object members.
    $this->configFactory = $config_factory;
    $this->tempstorePrivate = $tempstore_private->get('ws_php_vcr');
    $this->routeProvider = $route_provider;
    $this->pathMatcher = $path_matcher;
  }


  /**
   * Start listening incoming request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return bool|void
   * @throws \Dakwamine\Component\Exception\UnmetDependencyException
   */
  public function startListeningIncoming(Request $request) {
    $config_incoming = $this->configFactory->get('wsolution.incoming.settings');
    $start_listening = FALSE;

    // Listening on url settings.
    if (!empty($config_incoming->get('rest_url') && $this->pathMatcher->matchPath($request->getPathInfo(), $config_incoming->get('rest_url')))) {
      $start_listening = TRUE;
    }
    // Listening rest plugin automatically.
    if (!empty($config_incoming->get('rest_plugin')) && $config_incoming->get('rest_plugin') == TRUE) {
      $pp_rest_plugin = $this->tempstorePrivate->get('ws_rest_plugin');
      // Listening automatically all rest plugin.
      /** @var  $collection \Symfony\Component\Routing\RouteCollection */
      $collection = $this->routeProvider->getRouteCollectionForRequest($request);
      if ($routes = $collection->all()) {
        foreach ($routes as $route_name => $route) {
          if (strpos($route_name, 'rest') !== 0 || isset($pp_rest_plugin[$route_name])) {
            continue;
          }
          // Set in private temp store.
          $pp_rest_plugin[$route_name] = $route_name;
          $start_listening = TRUE;
        }
      }
      // Avoid recording multiple times.
      $this->tempstorePrivate->set('ws_rest_plugin', $pp_rest_plugin);
    }

    if ($start_listening) {
      /** @var  $passePlat  PassePlatComponent */
      $passePlat = ComponentBasedObject::getRootComponentByClassName(PassePlatComponent::class, TRUE);

      $passePlat->registerEventListeners([
        new EventListenerDefinition(
          ProcessSchemeEvent::EVENT_NAME,
          VCRSchemeProcessor::class
        ),
      ]);
      $passePlat->registerEventListeners([
        new EventListenerDefinition(
          ProcessedRequestEvent::EVENT_NAME
        )
      ]);

      //$passePlat->loadConfiguration();
      $passePlat->processStream();
    }
  }

  /**
   * Start listening outgoing request.
   *
   * @return bool
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function startListeningOutgoing() {
    $passeplat_settings = $this->configFactory->get('wsolution.outgoing.settings');
    $this->tempstorePrivate->set('ws_php_vcr_start', TRUE);
    // Start listening with PHP VCR.
    WSVcr::turnOn();
    WSVcr::configure()
      ->setCassettePath(DRUPAL_ROOT . $passeplat_settings->get('file_path'));
    $library = $passeplat_settings->get('library') ?? [
      'curl',
      'soap',
      'stream_wrapper',
    ];
    WSVcr::configure()->enableLibraryHooks($library);

    // Record requests and responses in cassette file.
    WSVcr::insertCassette(date('Y/m/d/H:i', time()) . '.' . $passeplat_settings->get('storage'));
    $config = $this->configFactory->get('wsolution.outgoing.settings');
    WSVcr::setMode($config->get('mode'));
    return TRUE;
  }

  /**
   * Stop listening outgoing request.
   *
   * @return bool
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function stopListeningOutgoing() {
    if ($this->tempstorePrivate->get('ws_php_vcr_start') === TRUE) {
      // To stop recording requests, eject the cassette.
      WSVcr::eject();
      // Turn off VCR to stop intercepting requests.
      WSVcr::turnOff();
      $this->tempstorePrivate->delete('ws_php_vcr_start');
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Stop listening incoming request.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function stopListeningIncoming() {
    $this->tempstorePrivate->delete('ws_rest_plugin');
  }

}

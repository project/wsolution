# WSolution Module

## Description

The WSolution module lets you modify CURL calls to transfer URLs called on our system. This makes it easy for you to use our SAAS system. A default configuration is proposed to manage caching, fallback and alerting. More information at [wsolution.io](https://www.wsolution.io).

## Dependencies

This module depends the PHP-VCR library:

- Dependency "php-vcr/php-vcr": "^1.6.7" ([GitHub php-vcr](https://github.com/php-vcr/php-vcr))
  - php-vcr:1.6.7 requires PHP version: `^8,<8.2 | >=8.2.9,<8.3`

Please use Composer to install the module.

## Installation

1. Use composer to install.
2. Navigate to "Extend" in the Drupal admin interface and enable the WSolution module.

## Getting started

After installation, navigate to "Admin > Configuration > Webservice > WSolution" to access the configuration page or with endpoint /admin/wsolution/settings/outgoing

Visit [wsolution.io](https://www.wsolution.io). Create your account quickly, then go to your profile page to retrieve your ID, then copy and paste it into the WSID field.

Add the url patterns you wish to pass to wsolution so that it can manage them for you. Enable the "include" option so that it's taken care of.

### Dev architecture

Using PHPVcr to override automatically functions of curl, soap, stream wrapper (VCR Directory) and redirect your calls through WSolution.

## License

This module is licensed under the GPL v2.0 or later.
